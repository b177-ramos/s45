import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
export default function Logout(){
	const { unsetUser, setUser } = useContext(UserContext);
	// localStorage.clear();
	// Clear the localStorage of the user's information
	unsetUser();
	// Adding useEffect that will allow the logout page to render first
	useEffect(() => {
		// Set the user state back to it's original value
		setUser({email: null});
	})
	// Redirect back to login
	return(
		<Navigate to='/login' />
	)
}

// import { userContext } from 'react';
// import { Navigate } from 'react-router-dom';
// import UserContext from '../UserContext';

// export default function Logout(){

// 	const { unsetUser, setUser } = useContext(UserContext);

// 	// localStorage.clear();
	
// 	// Clear the localStorage of the user's infromation
// 	unsetUser();

// 	// Adding useEffect that will allow the logout page to render first
// 	useEffect(() => {
// 		// Set the user state back to it's original value
// 		setUser({email: null});
// 	})

// 	// Redirect back to login
// 	return(
// 		<Navigate to='/login' />
// 	)
// }
// import Button from 'react-bootstrap/Button'
// // Bootstrap grid system components
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
// import { Col } from 'react-bootstrap';
// import { Row } from 'react-bootstrap';
import { Button, Row, Col } from 'react-bootstrap';


export default function Banner(){
	return (
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootstrap</h1>
				<p>Oppurtunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}
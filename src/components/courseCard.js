import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Card, Button } from 'react-bootstrap';


 export default function CourseCard({courseProp}) {
//     console.log(props);
//     console.log(typeof props);

    const {name, description, price} = courseProp;

    // Use the state hook for this component to be able to store its state
    // State are used to keep track of information related to individual components
    // Syntax:   when creating state

        //  const [getter, setter] = useState(initialGetterValue);
        // getter will store it, setter will update it

        const [count, setCount] = useState(0);
        console.log(useState(0));


        // Function that keeps track of enrollees for a course
        function enroll(){
            setCount(count + 1); // getter + 1
            console.log('Enrollees ' + count);
        }

        // 1. Create a seats state in the CourseCard component and set the initial value to 30.
        // 2. For every enrollment, deduct one to the seats.
        // 3. If the seats reaches zero do the following:
        //  - Do not add to the count.
        //  - Do not deduct to the seats.
        //  - Show an alert that says No more seats available.
        // Use state hook for getting and setting the seats for this course
        const [seats, setSeats] = useState(30);

        // Function that keeps track of enrollees for a course
        function enroll(){
            if(seats > 0){
                setCount(count + 1);
                console.log('Enrollees ' + count);
                setSeats(seats - 1);
                console.log('Seats' + seats);
            }
            else{
                alert("No more seats available");
            }
            
        }


    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Button variant="primary" onclick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}


// Check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
    // Properties to be validated
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })

}
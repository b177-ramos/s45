// import { Fragment } from 'react'
import { Fragment, useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Home from './pages/Home.js';
import Courses from './pages/Courses';
import Register from './pages/Register';
// import Logout from '/pages/Logout';
import Login from './pages/Login';
// import NotFound from './pages/NotFound';
import './App.css';
import { UserProvider } from './UserContext';


function App() {
  // State hook for the user state thats defined for a global scope
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }



  return (
    // user(email), setUser(logging in), unsetUser(logging out)
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />            
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
      </Router>
    </UserProvider>

    // <Router>
    //   <AppNavbar />
    //   <Container>
    //     <Routes>
    //       <Route path="/" element={<Home />} />
    //       <Route path="/courses" element={<Courses />} />
    //       <Route path="/login" element={<Login />} />
    //       <Route path="/register" element={<Register />} />
    //       <Route path="*" element={<NotFound />} />
    //     </Routes>
    //   </Container>
    // </Router>
    // <Fragment>
    //   <AppNavbar />
    //   <Container>
    //     // <Home />
    //     // <Courses />
    //     // <Register />
    //     < Login/>
    //   </Container>
    // </Fragment>
  );
}

export default App;